import {Cell} from '@/ts/Cell';
import {Direction, opposite, randomDirection} from '@/ts/Direction';
import {PathStrategy} from '@/ts/PathStrategy';

export class DrunkardWalkStrategy implements PathStrategy {
    private readonly corridorBias: number;

    private readonly totalRows: number;
    private readonly totalColumns: number;

    private readonly maxCenterBias: number;

    private currentRow: number;
    private currentColumn: number;
    private currentDirection: Direction;

    constructor(rows: number, columns: number, corridorBias: number, centerBias: number) {
        this.totalRows = rows;
        this.totalColumns = columns;
        this.corridorBias = corridorBias;
        this.maxCenterBias = centerBias;

        this.currentRow = Math.floor(this.totalRows / 2);
        this.currentColumn = Math.floor(this.totalColumns / 2);
        this.currentDirection = Direction.North;
    }

    public nextCell(): Cell {
        this.calculateNewDirection();
        this.move();
        return new Cell(this.currentRow, this.currentColumn);
    }

    private calculateNewDirection() {
        const centerBias = this.calcCenterBias();
        // centerBias and corridorBias are treated differently, which is not good,
        // but it's easier to handle edge cases like maxCenterBias = 0 and maxCenterBias = 1 this way
        if (Math.random() < centerBias) {
            // TODO this is a cheap way that works partially. better properly calculate quadrants
            this.currentDirection = opposite(this.currentDirection);
        } else if (Math.random() < (1 - this.corridorBias)) {
            this.currentDirection = randomDirection();
        } // otherwise continue moving in previous direction
    }

    private calcCenterBias() {
        const distanceFromCenterX = Math.abs(this.currentRow - this.totalRows / 2);
        const distanceFromCenterY = Math.abs(this.currentColumn - this.totalColumns / 2);
        const distanceFromCenter = Math.sqrt(Math.pow(distanceFromCenterX, 2) + Math.pow(distanceFromCenterY, 2));
        const maxDistanceFromCenter = Math.sqrt(Math.pow(this.totalRows / 2, 2) + Math.pow(this.totalColumns / 2, 2));
        return this.maxCenterBias * (distanceFromCenter / maxDistanceFromCenter);
    }

    private move() {
        switch (this.currentDirection) {
            case (Direction.North): {
                this.currentColumn = Math.max(this.currentColumn - 1, 0);
                break;
            }
            case (Direction.East): {
                this.currentRow = Math.min(this.currentRow + 1, this.totalRows - 1);
                break;
            }
            case (Direction.South): {
                this.currentColumn = Math.min(this.currentColumn + 1, this.totalColumns - 1);
                break;
            }
            case (Direction.West): {
                this.currentRow = Math.max(this.currentRow - 1, 0);
                break;
            }
        }
    }
}

