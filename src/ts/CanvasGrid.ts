import {CellType} from '@/ts/CellType';
import {Cell} from '@/ts/Cell';

export class CanvasGrid {
    private readonly ctx: CanvasRenderingContext2D;
    private readonly cellSize: number;

    constructor(canvas: HTMLCanvasElement, cellSize: number) {
        this.ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
        this.cellSize = cellSize;

        this.ctx.clearRect(0, 0, canvas.width, canvas.height);

        this.ctx.shadowColor = 'black';
        this.ctx.shadowBlur = 10;

        this.ctx.fillStyle = CellType.Background;
        this.ctx.fillRect(5, 5, canvas.width - 10, canvas.height - 10);

        this.ctx.shadowBlur = 0;
    }

    public fillCell(cell: Cell, type: CellType) {
        this.ctx.fillStyle = type.toString();
        this.ctx.fillRect(cell.row * this.cellSize, cell.column * this.cellSize,
            this.cellSize, this.cellSize);
    }
}
