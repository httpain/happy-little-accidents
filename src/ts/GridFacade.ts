import {Cell} from '@/ts/Cell';
import {CanvasGrid} from '@/ts/CanvasGrid';
import {CellType} from '@/ts/CellType';

export class GridFacade {
    private readonly rows: number;
    private readonly columns: number;

    private readonly canvasGrid: CanvasGrid;

    private readonly wallBorderSize: number;
    private readonly pathBorderProbabilities: Array<[number, number]>;

    private readonly data: CellType[][] = [];

    private reachableCells = 0;

    constructor(canvas: HTMLCanvasElement, cellSize: number,
                wallSize: number, pathBorderProbabilities: Array<[number, number]>) {
        this.rows = Math.floor(canvas.width / cellSize);
        this.columns = Math.floor(canvas.height / cellSize);

        this.canvasGrid = new CanvasGrid(canvas, cellSize);
        this.wallBorderSize = wallSize;
        this.pathBorderProbabilities = pathBorderProbabilities;

        this.prefillData();
    }

    // TODO implementation is easy to write and read, but inefficient
    public drawPathAt(origin: Cell) {
        const pathBorderSize = this.randomPathBorder(this.pathBorderProbabilities);
        const pathTotalSize = pathBorderSize * 2 + 1;
        const wallTotalSize = this.wallBorderSize * 2 + pathTotalSize;

        const wallCells: Cell[] = [];

        for (let i = 0; i < wallTotalSize; i++) {
            for (let j = 0; j < wallTotalSize; j++) {
                const row = origin.row - pathBorderSize - this.wallBorderSize + i;
                const column = origin.column - pathBorderSize - this.wallBorderSize + j;
                if (row >= 0 && row < this.rows
                    && column >= 0 && column < this.columns) {
                    wallCells.push(new Cell(row, column));
                }
            }
        }

        const pathCells: Cell[] = [];

        for (let i = 0; i < pathTotalSize; i++) {
            for (let j = 0; j < pathTotalSize; j++) {
                const row = origin.row - pathBorderSize + i;
                const column = origin.column - pathBorderSize + j;
                if (row >= 0 && row < this.rows
                    && column >= 0 && column < this.columns) {
                    pathCells.push(new Cell(row, column));
                }
            }
        }

        wallCells.forEach((cell) => this.updateCell(cell, CellType.Wall));
        pathCells.forEach((cell) => this.updateCell(cell, CellType.Path));
    }

    public reachableCellsAmount() {
        return this.reachableCells;
    }

    private updateCell(cell: Cell, newType: CellType) {
        const oldType = this.data[cell.row][cell.column];
        if (oldType !== CellType.Path
            && newType === CellType.Path) {
            this.reachableCells++;
        }

        if (this.shouldUpdateCell(oldType, newType)) {
            this.data[cell.row][cell.column] = newType;
            this.canvasGrid.fillCell(cell, newType);
        }
    }

    private shouldUpdateCell(oldType: CellType, newType: CellType): boolean {
        if (oldType === CellType.Path && newType !== CellType.Path) {
            // don't overwrite path cells, not onlu for visuals but also not to break reachableCells counter
            return false;
        } else {
            return true;
        }
    }

    private prefillData() {
        for (let i = 0; i < this.rows; i++) {
            this.data[i] = [];
            for (let j = 0; j < this.columns; j++) {
                this.data[i][j] = CellType.Background;
            }
        }
    }

    private randomPathBorder(pathSizeProbabilities: Array<[number, number]>): number {
        const random = Math.random();
        return pathSizeProbabilities.find((entry) => random >= entry[0])![1];
    }
}
