/* TODO because of the weirdness of TS/JS enums, can break if colors are the same
  (which should never happen but just in case) */
export enum CellType {
    Background = 'hsl(210, 20%, 15%)',
    Wall = 'hsl(210, 20%, 35%)',
    Path = 'hsl(210, 20%, 60%)'
}
