export enum Direction {
    North,
    East,
    South,
    West
}

export function opposite(direction: Direction): Direction {
    switch (direction) {
        case (Direction.North):
            return Direction.South;
        case (Direction.East):
            return Direction.West;
        case (Direction.South):
            return Direction.North;
        case (Direction.West):
            return Direction.East;
    }
}

export function randomDirection(): Direction {
    return Math.floor(Math.random() * Object.keys(Direction).length / 2);
}
