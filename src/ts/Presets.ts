class Preset {
    public readonly cellSize: number;
    public readonly iterationsPerFrame: number;
    public readonly fillFactor: number;
    public readonly corridorBias: number;
    public readonly centerBias: number;
    public readonly wallSize: number;
    public readonly pathBorderProbabilities: Array<[number, number]>;


    constructor(cellSize: number, iterationsPerFrame: number, fillFactor: number,
                corridorBias: number, centerBias: number,
                wallSize: number, pathBorderProbabilities: Array<[number, number]>) {
        this.cellSize = cellSize;
        this.iterationsPerFrame = iterationsPerFrame;
        this.fillFactor = fillFactor;
        this.corridorBias = corridorBias;
        this.centerBias = centerBias;
        this.wallSize = wallSize;
        this.pathBorderProbabilities = pathBorderProbabilities;
    }
}

const Presets = {
    OrganicBasic: new Preset(2, 200, 0.2,
        0.1, 0.1,
        1, [[0, 0]]),
    OrganicLarge: new Preset(2, 200, 0.25,
        0.2, 0.3,
        2, [[0, 1]]),
    Rooms: new Preset(2, 40, 0.15,
        0.95, 0.3,
        1, [[0.97, 5], [0, 1]])
};

export {Preset, Presets};
