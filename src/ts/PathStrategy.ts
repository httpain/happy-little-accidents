import {Cell} from '@/ts/Cell';

export interface PathStrategy {
    nextCell(): Cell;
}
