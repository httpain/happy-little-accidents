import {CellType} from '@/ts/CellType';

export class ProgressBar {
    private readonly ctx: CanvasRenderingContext2D;
    private readonly width: number;
    private readonly height: number;

    constructor(canvas: HTMLCanvasElement) {
        this.ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
        this.width = canvas.width;
        this.height = canvas.height;

        this.ctx.fillStyle = CellType.Background;
        this.ctx.fillRect(0, 0, canvas.width, canvas.height);
    }

    public updateProgress(current: number, max: number) {
        this.ctx.fillStyle = CellType.Path;
        this.ctx.fillRect(0, 0, Math.min(current / max, 1) * this.width, this.height);
    }

}
